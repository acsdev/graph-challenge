package graph.challenge.util;

import graph.challenge.entity.Graph;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseTest {

    Parse parse = new Parse();

    private Graph getGraph() {
        String input = Util.getFileFromResource("graph.in");
        return parse.getGraph(input);
    }

    @Test
    void shouldParseStringToGraph() {
        assertDoesNotThrow(() -> {
            Graph graph = getGraph();
            assertEquals(11, graph.getData().size());
        });
    }


    @Test
    void shouldReturnAdjacentList() {
        Graph graph = getGraph();
        Map<String, Set<String>> adjacentList = graph.getAdjacentList();
        assertEquals(2, adjacentList.get("A").size());
        assertEquals(3, adjacentList.get("B").size());
        assertEquals(3, adjacentList.get("C").size());
        assertEquals(1, adjacentList.get("D").size());
        assertEquals(2, adjacentList.get("E").size());
    }

    @Test
    @DisplayName("Find total distance from a given path")
    void testTotalDistance() {

    }

    @Test
    @DisplayName("Find available paths from a given pair")
    void testAvailablePaths() {

    }

    @Test
    @DisplayName("Find small path for a given pair")
    void testSmallPath() {

    }
}
