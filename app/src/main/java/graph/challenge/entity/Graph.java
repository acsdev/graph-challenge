package graph.challenge.entity;

import java.util.*;

public class Graph {

    private int id;

    private List<GraphData> data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<GraphData> getData() {
        return data;
    }

    public void setData(List<GraphData> data) {
        this.data = data;
    }

    public Map<String, Set<String>> getAdjacentList() {
        Map<String, Set<String>> map = new HashMap<>();
        List<GraphData> data = getData();

        for (GraphData graphData: data) {
            String source = graphData.getSource();
            String target = graphData.getTarget();
            int distance = graphData.getDistance();

            final String neighbor = String.format("%s,%s", target, distance);
            map.putIfAbsent(source, new HashSet<>());
            map.get(source).add(neighbor);
        }

        return map;
    }
}
