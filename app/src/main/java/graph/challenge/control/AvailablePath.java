package graph.challenge.control;

import graph.challenge.entity.Graph;

import java.util.ArrayList;
import java.util.List;

public class AvailablePath {

    /**
     * @param graph graph
     * @param beginNode starting point
     * @param endNode finishing point
     * @return list of possible path. first list has each path and the nested list has sequence node of each path
     */
    public List<List<String>> getDistance(Graph graph, String beginNode, String endNode) {
        return new ArrayList<>();
    }
}
