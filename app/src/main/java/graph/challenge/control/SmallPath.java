package graph.challenge.control;

import graph.challenge.entity.Graph;

import java.util.List;

public class SmallPath {

    /**
     * @param graph graph
     * @param beginNode starting point
     * @param endNode finishing point
     * @return small path between to nodes
     */
    public Result getDistance(Graph graph, String beginNode, String endNode) {
        return new Result();
    }

    public static class Result {
        private int distance;
        private List<String> orderedNodes;
    }
}
