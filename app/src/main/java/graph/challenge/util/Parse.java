package graph.challenge.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import graph.challenge.entity.Graph;

public class Parse {
    ObjectMapper objectMapper = new ObjectMapper();

    public Graph getGraph(String input) {
        try {
            return objectMapper.readValue(input, Graph.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
